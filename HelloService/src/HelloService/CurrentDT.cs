﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nancy;

namespace HelloService
{
    public class CurrentDT : NancyModule
    {
        public CurrentDT()
        {
            Get("/", _ => DateTime.UtcNow);
        }
    }
}
