﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nancy;
using Nancy.ModelBinding;

namespace ShoppingCartService.ShoppingCart
{
    public class ShoppingCartModule : NancyModule
    {
        public ShoppingCartModule(IShoppingCartStore shoppingCartStore) : base("/shoppingcart")
        {
            Get("/{userid:int}", parameters => {

                var userid = (int)parameters.userid;

                return shoppingCartStore.Get(userid);

            });
        }
    }
}
